#include "ofMain.h"
#include "ofBaseApp.h"

class App : public ofBaseApp
{
    private:

    public:
        void setup();
        void draw();
        void update();

};