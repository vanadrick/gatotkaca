#include "ofMain.h"

int main()
{
    ofSetupOpenGL(800, 600, OF_WINDOW);
    ofRunApp(new App());
    return 0;
}